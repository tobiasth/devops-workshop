import dao.UserDAO;
import data.User;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Before;
import org.junit.Test;
import resources.UserResource;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

public class UserResourceTest extends JerseyTest {
    private UserResource userResource;
    private UserDAO userDAO;
    private User user;
    private User user2;

    @Override
    public Application configure() {
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        return new ResourceConfig(UserResource.class);
    }

    @Before
    public void makeTestUser() {
        userResource = new UserResource();
        userDAO = new UserDAO();
        user = new User(10,"username","password",userDAO.generateSalt());
        user2 = new User();
    }

    @Test
    public void testNewUser(){
        Response output = target("/user").request().post(Entity.entity(user, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 200",200,output.getStatus());
        assertEquals("application/json",output.getHeaderString("Content-type"));
        assertEquals("username", userResource.newUser(user).getUsername());
    }

    @Test
    public void testGetUsers(){
        Response output = target("/user").request().get();
        assertEquals("should return status 200", 200, output.getStatus());
        assertNotNull("Should return user list", output.getEntity().toString());
        assertEquals("application/json",output.getHeaderString("Content-type"));
    }

    @Test
    public void testEditUser() {
        user2.setUsername("test");
        Response output = target("/user/" + user.getUserId() + "/").request().put(Entity.entity(user2, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 200", 200, output.getStatus());
        assertEquals("application/json",output.getHeaderString("Content-type"));
        assertNotEquals("testUsername",user2.getUsername());
    }
}
